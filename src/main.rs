
fn largest_copy<T: PartialOrd + Copy>(list: &[T]) -> T {
    let mut largest = list[0];

    for &item in list.iter() {
        if item > largest {
            largest = item;
        }
    }
    largest
}

fn largest_clone<T: PartialOrd + Clone>(list: &[T]) -> T {
    let mut largest = list[0].clone();

    for item in list.iter() {
        if item > &largest {
            largest = item.clone();
        }
    }

    largest
}

fn largest_borrow<T: PartialOrd>(list: &[T]) -> &T {
    let mut largest = &list[0];

    for i in 0..list.len() {
        if &list[i] > largest {
            largest = &list[i];
        }
    }

    largest
}

fn largest_slice<T: PartialOrd>(list: &[T]) -> &T {
    let mut largest = &list[0];
    for item in &list[..] {
        if item > largest {
            largest = item;
        }
    }
    largest
}

fn largest<T: PartialOrd >(list: &[T]) -> &T {
    let mut largest = &list[0];

    for item in list.iter() {
        if item > largest {
            largest = &item;
        }
    }
    largest
}



fn main() {
    let number_list = vec![34, 50, 25, 100, 65];

    let result = largest_clone(&number_list);
    println!("The largest number is {}", result);

    let char_list = vec!['y', 'm', 'a', 'q'];

    let result = largest_clone(&char_list);
    println!("The largest char is {}", result);

    let number_list = vec![34, 50, 25, 100, 65];

    let result = largest_copy(&number_list);
    println!("The largest number is {}", result);

    let char_list = vec!['y', 'm', 'a', 'q'];

    let result = largest_copy(&char_list);
    println!("The largest char is {}", result);

    let number_list = vec![34, 50, 25, 100, 65];

    let result = largest_borrow(&number_list);
    println!("The largest number is {}", result);

    let char_list = vec!['y', 'm', 'a', 'q'];

    let result = largest_borrow(&char_list);
    println!("The largest char is {}", result);


    let number_list = vec![34, 50, 25, 100, 65];

    let result = largest_slice(&number_list);
    println!("The largest number is {}", result);

    let char_list = vec!['y', 'm', 'a', 'q'];

    let result = largest_slice(&char_list);
    println!("The largest char is {}", result);


    let number_list = vec![34, 50, 25, 100, 65];

    let result = largest(&number_list);
    println!("The largest number is {}", result);

    let char_list = vec!['y', 'm', 'a', 'q'];

    let result = largest(&char_list);
    println!("The largest char is {}", result);



}